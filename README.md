# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignitionrobotics/ign-msgs

## Issues and pull requests are backed up at

https://osrf-migration.github.io/ignition-gh-pages/#!/ignitionrobotics/ign-msgs

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/ign-msgs

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

